<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Services\UserService;
use Inertia\Inertia;

class RegisterController extends Controller
{

    public function __construct(
        protected UserService $service
    ){}

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Register/Index', [
            'message' => "Register"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request, UserService $service)
    {
        $service->create($request->all());
        
        return to_route('login')->with('message', 'My message');
    }

}
